from django.contrib import admin
from .models import CustomerUserModel, ProductModel
# , MainUserModel, GeneralImageModel

admin.site.register(CustomerUserModel)
admin.site.register(ProductModel)
# admin.site.register(MainUserModel)
# admin.site.register(GeneralImageModel)