from django.shortcuts import render
from .models import CustomerUserModel, ProductModel

def index(request):
    return render(request, 'index.html', context = {
        "products":ProductModel.objects.all, 
        "customers":CustomerUserModel.objects.all,
    })