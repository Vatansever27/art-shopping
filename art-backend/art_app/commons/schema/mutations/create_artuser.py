import graphene
from graphene import relay
from ...models import MainUserModel
from ..types.inputs import ArtUserCreateInput
from ..types.nodes import ArtUserNode
from django.contrib.auth.decorators import login_required

class CreateArtUser(relay.ClientIDMutation):

    class Input:
        new_artuser_details = graphene.Field(CustomerUserCreateInput)

    created_artuser_details = graphene.Field(CustomerUserNode)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        created_artuser_details = MainUserModel.objects.create(**input.get("new_customer_details"))
        print("CCD : ", created_customer_details.image)
        file = info.context.FILES['image']
        print("file : ", file)
        created_customer_details.image = file
        return CreateCustomer(created_customer_details=created_customer_details)
