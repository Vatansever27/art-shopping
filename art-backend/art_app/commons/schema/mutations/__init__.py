import graphene
from .create_customer import CreateCustomer, CreateImage

class Mutation(graphene.ObjectType):
    create_customer = CreateCustomer.Field()
    create_image = CreateImage.Field()