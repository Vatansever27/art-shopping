import graphene
from graphene import relay, ObjectType
from graphene_django.filter import DjangoFilterConnectionField
from .types.nodes import ArtUserUserNode, GeneralImageNode

class MainUserQuery(ObjectType):
    customer = relay.Node.Field(CustomerUserNode)
    all_customers = DjangoFilterConnectionField(CustomerUserNode)

class GeneralImageQuery(ObjectType):
    image = relay.Node.Field(ArtUserNode)
    all_images = DjangoFilterConnectionField(GeneralImageNode)

class Query(
    MainUserQuery, graphene.ObjectType,
    GeneralImageQuery, graphene.ObjectType,
):
    pass