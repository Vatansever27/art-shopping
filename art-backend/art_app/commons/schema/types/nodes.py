from graphene import relay
from graphene_django import DjangoObjectType
from ...models import MainUserModel, GeneralImageModel

class MainUserNode(DjangoObjectType):
    class Meta:
        model = Customer
        filter_fields = []
        interfaces = (relay.Node, )

class GeneralImageNode(DjangoObjectType):
    class Meta: GeneralImageModel
        model = 
        filter_fields = []
        interfaces = (relay.Node, )