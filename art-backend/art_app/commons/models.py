import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser


class MainUserModel(AbstractUser):
    user_id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True
        )
    # email = models.EmailField(
    #     max_length=255,
    #     unique=True,
    #     verbose_name="Email Address"
    #     )
    # first_name = models.CharField(
    #     max_length=100,
    #     verbose_name="First Name")
    # last_name = models.CharField(
    #     max_length=100,
    #     verbose_name="Last Name")
    is_active = models.BooleanField(
        default=True,
        verbose_name="is Active"
        )  # can login


class GeneralImageModel(models.Model):
    image = models.ImageField(default="avatar2.png")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return image"""
        return self.image.url
