import uuid
from django.db import models
from art_app.commons.models import MainUserModel, GeneralImageModel
from phonenumber_field.modelfields import PhoneNumberField

class CustomerUserModel(models.Model):
    # user    =   models.OneToOneField(MainUserModel, on_delete=models.CASCADE, verbose_name="User Model Details")
    # user = models.UUIDField(
    #     primary_key=True,
    #     default=uuid.uuid4,
    #     editable=False,
    #     unique=True
    #     )
    # guid = models.IntegerField(primary_key=True, editable=False, default=-1) 
    email = models.EmailField(
        max_length=255,
        unique=True,
        verbose_name="Email Address"
        )
    first_name = models.CharField(
        max_length=100,
        verbose_name="First Name")
    last_name = models.CharField(
        max_length=100,
        verbose_name="Last Name")
    created_at   =   models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # image = models.OneToOneField(GeneralImageModel, on_delete=models.CASCADE, verbose_name="Image")
    image = models.ImageField(default="avatar2.png", verbose_name="Image")
    shipping_address = models.TextField(verbose_name="Shipping Address")
    invoice_address = models.TextField(verbose_name="Invoice Address")
    phone_number = PhoneNumberField(verbose_name="Contact Number")
    other_notes = models.CharField(max_length=255, verbose_name="Order Notes")

    is_active = models.BooleanField(
        default=True,
        verbose_name="is Active"
        )
        
    # GENDER = [
    #     ("male", "Erkek"),
    #     ("female", "Kadın")
    # ]
    # gender = models.CharField(max_length=6, choices=GENDER, blank=True, null=True)

    def __str__(self):
        return self.first_name

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ""
        return url
