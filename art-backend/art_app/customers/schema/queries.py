import graphene
from graphene import relay, ObjectType
from graphene_django.filter import DjangoFilterConnectionField
from .types.nodes import CustomerUserNode

class CustomerUserQuery(ObjectType):
    customer_user = relay.Node.Field(CustomerUserNode)
    all_customer_users = DjangoFilterConnectionField(CustomerUserNode)

class Query(
    CustomerUserQuery, graphene.ObjectType,
):
    pass