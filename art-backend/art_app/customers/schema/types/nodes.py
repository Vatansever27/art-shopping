from graphene import relay
from graphene_django import DjangoObjectType
from ...models import CustomerUserModel

class CustomerUserNode(DjangoObjectType):
    class Meta:
        model = CustomerUserModel
        filter_fields = []
        interfaces = (relay.Node, )
