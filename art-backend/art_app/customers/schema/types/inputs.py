import graphene

class CustomerUserCreateInput(graphene.InputObjectType):
    first_name = graphene.String(required=True)
    last_name = graphene.String(required=True)
    email = graphene.String(required=True)
    first_name = graphene.String()
    last_name = graphene.String()
    shipping_address = graphene.String()
    invoice_address = graphene.String()
    phone_number = graphene.String()
    other_notes = graphene.String()
    