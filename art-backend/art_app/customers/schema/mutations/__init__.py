import graphene
from .create_customer import CreateCustomerUser
from .delete_customer import DeleteCustomerUser


class Mutation(graphene.ObjectType):
    create_customer_user = CreateCustomerUser.Field()
    delete_customer_user = DeleteCustomerUser.Field()
