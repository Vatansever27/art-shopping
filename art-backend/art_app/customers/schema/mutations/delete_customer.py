import graphene
from graphene import relay
from ...models import CustomerUserModel
from ..types.inputs import CustomerUserCreateInput
from ..types.nodes import CustomerUserNode
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from graphql import GraphQLError


class DeleteCustomerUser(relay.ClientIDMutation):

    class Input:
        id = graphene.ID(required=True)

    deleted_customer_details = graphene.Field(CustomerUserNode)

    @classmethod
    def mutate_and_get_payload(cls, root, info, id):
        deleted_customer_details = get_object_or_404(CustomerUserModel, id=id)
        if deleted_customer_details.is_active:
            deleted_customer_details.is_active = False
            deleted_customer_details.save()
        else:
            raise GraphQLError("This user is not active!")

        return DeleteCustomerUser(deleted_customer_details=deleted_customer_details)
