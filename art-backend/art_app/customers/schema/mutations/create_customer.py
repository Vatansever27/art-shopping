import graphene
from graphene import relay
from ...models import CustomerUserModel
from ..types.inputs import CustomerUserCreateInput
from ..types.nodes import CustomerUserNode
from django.contrib.auth.decorators import login_required

class CreateCustomerUser(relay.ClientIDMutation):

    class Input:
        new_customer_details = graphene.Field(CustomerUserCreateInput)

    created_customer_details = graphene.Field(CustomerUserNode)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        created_customer_details = CustomerUserModel.objects.create(**input.get("new_customer_details"))
        file = info.context.FILES['image']
        created_customer_details.image = file
        return CreateCustomer(created_customer_details=created_customer_details)
