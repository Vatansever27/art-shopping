import uuid
from django.conf import settings
from djmoney.models.fields import MoneyField
from django.db import models
from art_app.customers.models import CustomerUserModel, GeneralImageModel


class ProductModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    whose = models.ForeignKey(CustomerUserModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, )
    description = models.CharField(max_length=500)
    image = models.OneToOneField(GeneralImageModel, on_delete=models.CASCADE, verbose_name="Image")
    PRODUCT_TYPE = (
        ('painting', 'painting'),
        ('accessory', 'accessory'),
        ('card', 'card'),
        ('other', 'other')
    )
    productType = models.CharField(max_length=100, choices=PRODUCT_TYPE)
    price = MoneyField(max_digits=7, decimal_places=2, default_currency='Turkish Lira')
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)