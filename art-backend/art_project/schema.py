import graphene
import art_app.customers.schema as customer
# import art_app.commons.schema as common

class Query(customer.Query, graphene.ObjectType):
    pass

class Mutation(customer.Mutation, graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query, mutation=Mutation)
