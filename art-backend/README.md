# BACKEND
Art Shopping backend side

## Install dependencies
pipenv shell

pipenv install

(
    python 3.6

    django 3.0.7
    
    django-cors-headers ==>  a Django application for handling the server headers required for Cross-Origin Resource Sharing (CORS).
    
    graphene = ">=2.0" ==> Graphene provides some additional abstractions that make it easy to add GraphQL functionality to your Django project.
    
    graphene-django ==> Graphene-Django is built on top of Graphene. 
    
    pillow ==> Pillow is the friendly PIL fork, and PIL is the Python Imaging Library
    
    django-cleanup ==> The django-cleanup app automatically deletes files for FileField, ImageField and subclasses.
)

## SQLITE
python manage.py makemigrations ==> to create model columns in db

python manage.py migrate

# to run the project on local:8000 port
python manage.py runserver
