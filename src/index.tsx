import React from 'react';
import ReactDOM from 'react-dom';
import Sepetim from './Pages/Sepetim';
import Kombine from './Pages/Kombine';
import HomePage from './Pages/HomePage';
import Hakkımızda from './Pages/Hakkımızda';
import Kampanyalar from './Pages/Kampanyalar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import * as serviceWorker from './serviceWorker';
import Ürünler from './Pages/Urunler/Urunler.container';
import Navbar from './Common/Navbar/Navbar.component';
import UrunlerDetail from './Pages/Urunler/Urunler.detail';
import 'semantic-ui-css/semantic.min.css'
import { Provider } from 'react-redux';
import { store } from './redux/store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
      <Navbar/>
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/ürünler" component={Ürünler}/>
          <Route exact path="/hakkımızda" component={Hakkımızda}/>
          <Route exact path="/kampanyalar" component={Kampanyalar}/>
          <Route exact path="/sepetim" component={Sepetim}/>
          <Route exact path="/kombine" component={Kombine}/>
          <Route exact path="/urunler-detail/:id" component={UrunlerDetail}/>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
