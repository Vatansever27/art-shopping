import React, { FunctionComponent } from 'react';
import './Card.css';
import '../../Pages/Urunler/Urunler.css';
import { Link } from 'react-router-dom';

interface Props{
  img: string
  text:string
  name:string
  spend:number
  id: number
  type?:string
}

const Card: FunctionComponent<Props> = ({ img, name, text, spend, id, type }) => {
  const data = { img, name, text, spend, id, type };
  return (
    <div className="card">
      <img src={img} style={{padding:'20px'}} className="card-img-top" alt="..."/>
      <div className="card-body">
        <h3 className="card-title">{name}</h3>
        <p className="text">{text}</p>
        <p className="text">{spend} TL</p>
        <div className="button-container">
          <Link
            className="btn btn-dark"
            to={{
              pathname:`urunler-detail/${id}`,
              state: {
                data
              }
            }}>Incele
          </Link>
        </div>  
      </div>
    </div>
  );
};

export default Card;
