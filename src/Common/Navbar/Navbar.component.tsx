import React, { useState } from 'react';
import {Link}  from 'react-router-dom';
import './Navbar.css';
import Logo from '../../styles/images/zeytin-dal.png';
import { Menu, Segment } from 'semantic-ui-react'
import { useSelector } from 'react-redux';

function Navbar() {
  const [activeItem, setActiveItem] = useState({activeItem: 'Ana Sayfa'});
  const { name } = activeItem.activeItem as any
  const handleItemClick = (e:any,  name:any) => {
    setActiveItem({activeItem: name})
  }
  const reduceState = useSelector((state:any)=> state.app);

  return (
    <div className="Navbar">
      <div className="Logo">
        <a href="/">
        <img className="LogoImg" alt="logo" src={Logo}/>
        </a>
      </div>
      <div className="Nav-Items">
        <Segment inverted>
          <Menu inverted pointing secondary>
          <Link to="/">
            <Menu.Item
              name='Ana Sayfa'
              active={name === "Ana Sayfa"}
              onClick={handleItemClick}
            />
            </Link>
            <Link to="/hakkımızda">
            <Menu.Item
              name='hakkımızda'
              active={name === "hakkımızda"}
              onClick={handleItemClick}
            />
            </Link>
            <Link to="/ürünler">
            <Menu.Item
              name='Ürünler'
              active={name === 'ürünler'}
              onClick={handleItemClick}
            />
            </Link>
            <Link to="/kampanyalar">
            <Menu.Item
              name='kampanyalar'
              active={name === 'kampanyalar'}
              onClick={handleItemClick}
            />
            </Link>
            <Link to="/kombine">
            <Menu.Item
              name='kombine'
              active={name === 'kombine'}
              onClick={handleItemClick}
            />
            </Link>
            <Link to="/sepetim">
            <Menu.Item
              name='sepetim'
              active={name === 'sepetim'}
              onClick={handleItemClick}
            >Sepetim {reduceState.items.length}</Menu.Item>
            </Link>
          </Menu>
        </Segment>
      </div>
    </div>
  );
};

export default Navbar;
