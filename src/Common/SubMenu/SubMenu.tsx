import React, { useState, FunctionComponent } from 'react';
import './SubMenu.css';
import { Menu, Segment } from 'semantic-ui-react'

interface Props{
  setState: any
}

const SubMenu: FunctionComponent<Props> = ({setState}) => {
  const [activeItem, setActiveItem] = useState({activeItem: 'Hepsi'});
  const handleItemClick = (e:any,  name:any) => {
    setActiveItem({activeItem: name})
    setState(name);
  }
  const { name } = activeItem.activeItem as any
  return (
    <div className="submenu-items">
      <Segment inverted>
        <Menu inverted pointing secondary>
          <Menu.Item
            style={{fontSize: '12px !important'}}
            name='Hepsi'
            active={activeItem.activeItem === "Hepsi" || name === 'Hepsi'}
            onClick={handleItemClick}
          />
          <Menu.Item
            style={{fontSize: '12px !important'}}
            name='Tablo'
            active={activeItem.activeItem === "Tablo" || name === 'Tablo'}
            onClick={handleItemClick}
          />
          <Menu.Item
            name='Kart Postallar'
            active={name === 'Kart Postallar'}
            onClick={handleItemClick}
          />
          <Menu.Item
            name='Kolyeler'
            active={name === 'Kolyeler'}
            onClick={handleItemClick}
          />
          <Menu.Item
            name='Taş Boyama'
            active={name === 'Taş Boyama'}
            onClick={handleItemClick}
          />
        </Menu>
      </Segment>
    </div>
  );
};

export default SubMenu;
