import React from 'react';
import '../styles/Hakkımızda.css';

function Hakkımızda() {
  const Background = require('../styles/images/hakkimizda-bg.jpg')
  return (
    <div 
      className="hakkimizda" 
      style={{backgroundImage: `url(${Background})`,
      }}
    >
      <div className="hakkimizda-title">Hakkımızda</div>
        <div className="hakkimizda-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rutrum massa id libero consectetur placerat. In magna enim, porta sed pretium id, lacinia et leo. Mauris feugiat augue eu felis mattis, eget sollicitudin orci suscipit. Integer sed placerat dui. Cras molestie iaculis eleifend. Vivamus ut fringilla augue. Nulla augue urna, laoreet quis efficitur a, vestibulum in sem. Proin vel vehicula metus. Integer eget posuere enim. Duis vehicula dignissim erat sit amet vestibulum. Curabitur eget magna tincidunt, tempor turpis quis, semper metus.</div>
        <div className="hakkimizda-text">In gravida neque ex, vitae tincidunt massa rhoncus vitae. In malesuada enim vitae mattis volutpat. Integer tristique viverra libero, in suscipit sem rutrum at. Etiam vitae nunc est. Vestibulum consectetur elit ac leo semper consequat. Integer consectetur euismod libero a tempor. Donec nisl eros, lobortis sed massa ut, posuere porttitor quam. Nullam feugiat ut lorem vel hendrerit. Ut porttitor dolor eu eros maximus, eu dictum nulla ultricies. Aliquam rhoncus posuere erat, at pharetra est pellentesque quis. Nunc pretium urna vel eros dictum egestas.</div>
        <div className="hakkimizda-text">Suspendisse sollicitudin non ligula quis molestie. Nunc pharetra convallis quam in semper. Nam dapibus elementum massa. In ipsum elit, consequat vel risus nec, tristique scelerisque massa. Cras ut eros sed orci ultricies gravida. Vivamus vestibulum urna vitae gravida cursus. Curabitur rhoncus convallis leo et vehicula. Maecenas faucibus lacus ante, vel fringilla enim varius in. In tincidunt erat et orci viverra placerat. Sed purus orci, dapibus et dui eu, imperdiet hendrerit mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce neque risus, rutrum in mi egestas, tristique egestas enim. Aenean vitae felis ut odio suscipit tincidunt.</div>
    </div>
  );
};

export default Hakkımızda;

// styles={{backgroundImage: `url(${Background})`  }}