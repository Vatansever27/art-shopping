import React from 'react';
import '../styles/Sepetim.css';
import { useSelector, useDispatch } from 'react-redux';
import CloseImg from '../styles/images/close-button.png';

function Sepetim() {
  const dispatch = useDispatch();
  const reduceState = useSelector((state:any)=> state.app);
  const func = (id:any) => {
    dispatch({type:"DELETE_ITEM", id})
  }
  console.log(reduceState.items.length);
  return (
    <div className="sepetim">
      {
        reduceState.items.length === 0 ? 
        <div className="ui purple big message">Sepetinizde henüz bir ürün bulunmamaktadır.</div>
      :
      reduceState.items.map((item:any,index:number) => (
        <div key={index} className="sepetim-detail-container">
          <img src={item.img} alt="Close Button" className="sepetim-detail-img"/>
          <div className="sepetim-detail-text">
            <div className="sepetim-title">
              Isim: {item.name}
            </div>
            <div className="sepetim-text">
              Detay: {item.text}
            </div>
            <div className="sepetim-spend">
              Fiyat: {item.spend}
            </div>
          </div>
          <div className="sepetim-delete">
            <img onClick={(e) => func(item.id)} src={CloseImg} alt="Close Button" className="sepetim-delete-img"/>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Sepetim;
