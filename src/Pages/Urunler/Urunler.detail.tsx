import React, { FunctionComponent, useState } from 'react';
import './Urunler.css';
import { useLocation } from 'react-router-dom';
import ReactImageMagnify from 'react-image-magnify';
import staffList from '../../data/urunler';
import {Formik, ErrorMessage} from 'formik';
import * as yup from 'yup';
import { Dropdown, Button, Form, Input } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';

const necklesSizeImg = require('../../styles/neckles/necklesSizeImg.png');

const validationSchema = yup.object({
  chainSize: yup.string(),
  envelopeText: yup.string()
});

const UrunlerDetail:FunctionComponent= () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const [state, setState] = useState(false);
  const { data } = location.state as any
  const initvalues = { chainSize: '', envelopeText: ''};

  const addItem = () => {
    dispatch({type:'ADD_ITEM', data})
    setState(true);
    setTimeout(() => {
    setState(false);
    }, 3500);
  }
  return (
    <div className="urunler-detail-container">
      {
        state === false ? null : <div className="ui purple big message">{data.name}  Sepete Eklendi.</div>
      }
      <div className="urunler-detail">
        <ReactImageMagnify className="image-magnify" {...{
          smallImage: {
              alt: '...',
              isFluidWidth: true,
              src: data.img,
              sizes: '(max-width: 480px) 100vw, (max-width: 1200px) 30vw, 360px'
          },
          largeImage: {
              src: data.img,
              width: 1290,
              height: 1700
          },
          isHintEnabled: true,
          shouldUsePositiveSpaceLens: true,
          enlargedImagePosition: 'over',
        }} />
        <div className="urunler-detail-text-container">
          <div className="urunler-detail-title">{data.name}</div>
          <div className="urunler-detail-text">{data.text}</div>
          <div className="urunler-detail-text">{data.spend} TL</div>
            <div>
              <Formik
              validationSchema={validationSchema}
              initialValues={initvalues}
              onSubmit={(values, { resetForm }) => {
                resetForm();
                console.log({ values });
                // alert(JSON.stringify(values, null, 2));
              }}>
              {({ handleSubmit, setFieldValue, setFieldTouched, handleChange, handleBlur }) => (
                <Form onSubmit={handleSubmit}>
                  {
                    data.type === 'kolye' ?
                    <div className="urunler-detail-dropdown-container">
                      <div>
                        <Dropdown
                          className="urunler-detail-dropdown"
                          selection
                          options={staffList.KolyeOptions}
                          placeholder="Zincir Uzunlugu"
                          name="chainSize"
                          onBlur={() => setFieldTouched('chainSize', true)}
                          onChange={(e, { value }) =>
                            setFieldValue('chainSize', value)
                          }
                        />
                        <div className="dropdown-error">
                          <ErrorMessage name="chainSize" />
                        </div>
                      </div>
                      <img className="urunler-detail-neckles-size-img" src={necklesSizeImg} alt="sizeImg"/>
                    </div>
                  : null  
                  }
                  {
                    data.type === 'envelope' ?
                    <div className="urunler-detail-envelope-text-container">
                      <label htmlFor="email">Kendi Kart Postalini Yarat.</label>
                      <Input
                        id="note"
                        name="envelopeText"
                        type="textarea"
                        placeholder="..."
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                    :
                    null
                  }
                  <div className="urunler-detail-button">
                    <Button className="modal-button" onClick={addItem}>
                      SEPETE EKLE
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UrunlerDetail;


// Dilara: Klasik waffle, Nutella, Sütlü, Beyaz, Muz, Çilek, Kivi, Antep fıstıklı, Damla çikolta sütlü, Kırık Fındık, Bütün antep fıstığı, İncir
// Doğu: Klasik waffle full beyaz, Muz, Çilek, Antep fıstıklı, Bütün Antep Fıstığı, Toz Antep Fıstığı
// {"\u2728"}

// Double sorulacak