import React, { FunctionComponent } from 'react';
import './Urunler.css';
import Card from '../../Common/Card/Card.component';

interface Props{
  data: any
  title?: string
}

const ÜrünlerComponent: FunctionComponent<Props> = ({data,title}) => {
  return (
    <div className="urunler-container">
       <div className="title">{title}</div>
       <div className="Urunler">
       {data.map((item:any,index:number) => (
        <div className="card-detail" key={index}>
          <Card 
            img={item.img}
            text={item.text}
            name={item.name}
            spend={item.spend}
            id={item.id}
            type={item.type}
          />
        </div>
        ))}
      </div>
    </div>
  );
};

export default ÜrünlerComponent;
