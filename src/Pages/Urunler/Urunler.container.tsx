import React, { useEffect, useState } from 'react';
import './Urunler.css';
import ÜrünlerComponent from './Ürünler.component';
import staffList from '../../data/urunler';
import allStaff from '../../data/homepage';
import SubMenu from '../../Common/SubMenu/SubMenu';

function Ürünler() {
  const [state, setState] = useState('all');
  const [change, setChange] = useState<any[]>(allStaff);
  const {name} = state as any;
  const changer:any = (state:any) => {
    if(state === "Hepsi"){
      setChange(allStaff);
    }
    if(state === "Tablo"){
      setChange(staffList.staffList);
    }
    if(state === "Kart Postallar"){
      setChange(staffList.envelopeStaff);
    }
    if(state === 'Kolyeler'){
      setChange(staffList.necklesStaff);
    }
    if(state === 'Taş Boyama'){
      setChange(staffList.stonePaintingStaff);
    }
  }
  useEffect(() => {
    changer(name)
  }, [name])
  return (
    <div>
      <SubMenu
        setState={(state:any)=> setState(state)}
      />
      <ÜrünlerComponent
        data={change}
      />
    </div>
  );
};

export default Ürünler;
