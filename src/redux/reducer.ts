const initState ={
  items : [],
  anchorArray: []
}

const SET_INITIAL_DATA = "SET_INITIAL_DATA";
const ADD_ITEM = "ADD_ITEM";
const DELETE_ITEM = "DELETE_ITEM";

export const reducer = (
  state:any = initState,
  action:any) => {
  switch (action.type) {
    case SET_INITIAL_DATA:
      console.log('SET_INITIAL_DATA:',state);
      return {
        state
      };
    case ADD_ITEM:
      return {
        ...state,
        items:[
          ...state.items,
          action.data
        ]
      };
    case DELETE_ITEM:
      const itemDeleteIndex = state.items.findIndex((elem:any) => elem.id === action.id);
      console.log('itemDeleteIndex',itemDeleteIndex);
      return {
        ...state,
        items:[
          ...state.items.slice(0, itemDeleteIndex),
          ...state.items.slice(itemDeleteIndex + 1)
        ],
      };
    default:
      return state;
  }
}
