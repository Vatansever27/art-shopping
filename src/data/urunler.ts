const tablo1Img1 = require('../styles/painting/tablo1.jpeg');
const tablo1Img2 = require('../styles/painting/tablo2.jpeg');
const tablo1Img3 = require('../styles/painting/tablo3.jpeg');
const tablo1Img4 = require('../styles/painting/tablo4.jpeg');
const tablo1Img5 = require('../styles/painting/tablo5.jpeg');
const tablo1Img6 = require('../styles/painting/tablo6.jpeg');

const envelope1 = require('../styles/enveloperImg/envelope1.jpeg');
const envelope2 = require('../styles/enveloperImg/envelope2.jpeg');

const neckles1 = require('../styles/neckles/kolye1.jpg');
const neckles2 = require('../styles/neckles/kolye2.jpg');
const neckles3 = require('../styles/neckles/kolye3.jpg');
const neckles4 = require('../styles/neckles/kolye4.jpg');
const neckles5 = require('../styles/neckles/kolye5.jpg');
const neckles6 = require('../styles/neckles/kolye6.jpg');
const neckles7 = require('../styles/neckles/kolye7.jpg');
const neckles8 = require('../styles/neckles/kolye8.jpg');
const neckles9 = require('../styles/neckles/kolye9.jpg');
const neckles10 = require('../styles/neckles/kolye10.jpg');
const neckles11 = require('../styles/neckles/kolye11.jpg');
const neckles12 = require('../styles/neckles/kolye12.jpg');

const stonePainting1 = require('../styles/stonePainting/tasboyama1.jpeg');
const stonePainting2 = require('../styles/stonePainting/tasboyama2.jpeg');
const stonePainting3 = require('../styles/stonePainting/tasboyama3.jpeg');
const stonePainting4 = require('../styles/stonePainting/tasboyama4.jpg');
const stonePainting5 = require('../styles/stonePainting/tasboyama5.jpg');
const stonePainting6 = require('../styles/stonePainting/tasboyama6.jpeg');
const stonePainting7 = require('../styles/stonePainting/tasboyama7.jpeg');
const stonePainting8 = require('../styles/stonePainting/tasboyama8.jpeg');
const stonePainting9 = require('../styles/stonePainting/tasboyama9.jpeg');
const stonePainting10 = require('../styles/stonePainting/tasboyama10.jpeg');
const stonePainting11 = require('../styles/stonePainting/tasboyama11.jpeg');
const stonePainting12 = require('../styles/stonePainting/tasboyama12.jpeg');

const staffList = [
  {
    id: 1,
    img: tablo1Img1,
    name: 'TABLO 1',
    spend: 280,
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
  },
  {
    id: 2,
    img: tablo1Img2,
    name: 'TABLO 2',
    spend: 260,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 3,
    img: tablo1Img3,
    name: 'Tablo 3',
    spend: 300,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 4,
    img: tablo1Img4,
    name: 'Tablo 4',
    spend: 280,
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
  },
  {
    id: 5,
    img: tablo1Img5,
    name: 'Tablo 5',
    spend: 260,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 6,
    img: tablo1Img6,
    name: 'Tablo 6',
    spend: 300,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
];

const envelopeStaff = [
  {
    id: 1,
    img: envelope1,
    name: 'Kart Postal 1',
    spend: 20,
    type: 'envelope',
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
  },
  {
    id: 2,
    img: envelope2,
    name: 'Kart Postal 2',
    spend: 20,
    type: 'envelope',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
];

const necklesStaff = [
  {
    id: 1,
    img: neckles1,
    name: 'Kolye 1',
    spend: 60,
    type:'kolye',
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
  },
  {
    id: 2,
    img: neckles2,
    name: 'Kolye 2',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 3,
    img: neckles3,
    name: 'Kolye 3',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 4,
    img: neckles4,
    name: 'Kolye 4',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 5,
    img: neckles5,
    name: 'Kolye 5',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 6,
    img: neckles6,
    name: 'Kolye 6',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 7,
    img: neckles7,
    name: 'Kolye 7',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 8,
    img: neckles8,
    name: 'Kolye 8',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 9,
    img: neckles9,
    name: 'Kolye 9',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 10,
    img: neckles10,
    name: 'Kolye 10',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 11,
    img: neckles11,
    name: 'Kolye 11',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 12,
    img: neckles12,
    name: 'Kolye 12',
    spend: 60,
    type:'kolye',
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
];

const stonePaintingStaff = [
  {
    id: 1,
    img: stonePainting1,
    name: 'Taş Boyama 1',
    spend: 60,
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
  },
  {
    id: 2,
    img: stonePainting2,
    name: 'Taş Boyama 2',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 3,
    img: stonePainting3,
    name: 'Taş Boyama 3',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 4,
    img: stonePainting4,
    name: 'Taş Boyama 4',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 5,
    img: stonePainting5,
    name: 'Taş Boyama 5',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 6,
    img: stonePainting6,
    name: 'Taş Boyama 6',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 7,
    img: stonePainting7,
    name: 'Taş Boyama 7',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 8,
    img: stonePainting8,
    name: 'Taş Boyama 8',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 9,
    img: stonePainting9,
    name: 'Taş Boyama 9',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 10,
    img: stonePainting10,
    name: 'Taş Boyama 10',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 11,
    img: stonePainting11,
    name: 'Taş Boyama 11',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    id: 12,
    img: stonePainting12,
    name: 'Taş Boyama 12',
    spend: 60,
    text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
];

const KolyeOptions = [
  { key: 1, value: '35.5cm', text: '35.5cm' },
  { key: 2, text: '40.6cm', value: '40.6cm' },
  { key: 3, text: '45.7cm', value: '45.7cm,' },
  { key: 4, text: '50.8cm', value: '50.8cm' },
  { key: 5, text: '55.9cm', value: '55.9cm' },

];

export default {envelopeStaff, staffList, necklesStaff, stonePaintingStaff,KolyeOptions};